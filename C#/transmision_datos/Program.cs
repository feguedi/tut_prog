using System;
using System.Net;
using System.Net.Sockets;

namespace transmision_datos
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket miPrimerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint direccion = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1234);

            miPrimerSocket.Bind(direccion);
            miPrimerSocket.Listen(5);

            Socket Escuchar = miPrimerSocket.Accept();
            System.Console.WriteLine("Conexión con éxito...");

            byte[] ByRec = new byte[255];
            int a = Escuchar.Receive(ByRec, 0, ByRec.Length, 0);

            Array.Resize(ref ByRec, a);

            System.Console.WriteLine("Cliente dice " + Encoding.Default.GetString(ByRec));
            miPrimerSocket.Close();
            
            String info = Console.ReadLine();
            byte[] infoEnviar = Encoding.Default.GetBytes(info);

            //miPrimerSocket.Send(infoEnviar, 0, infoEnviar.Length, 0);
            //miPrimerSocket.Send(infoEnviar, 0, infoEnviar.Length, 0);
            //miPrimerSocket
        }
    }
}