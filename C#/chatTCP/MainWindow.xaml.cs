using System;
using System.Text;
using System.Windows;
using System.Net.Sockets;
using System.Net.Sockets.SocketException;

public partial class MainWindow : Windows
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
        if (txtDir.Text.Length > 0 && txtPort.Text.Length > 0)
        {
            TcpClient socket = new TcpClient(txtDir.Text, Int32.Parse(txtPort.Text));
            NetworkStream ns = socketGetStream();

            string msg = "j^" + txtUser.Text + "@localhost^-^-^";

            byte[] buffer = new byte[msg.Length];
            buffer = ASCIIEncoding.ASCII.GetBytes(msg);

            ns.Write(buffer, 0, buffer.Length);
            buffer = new byte[255];
            ns.Read(buffer, 0, 255);
            msg = ASCIIEncoding.ASCII.GetString(buffer).Trim();

            txtMain.Text = msg.Trim();
        }
    }
}