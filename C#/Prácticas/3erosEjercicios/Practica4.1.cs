using System;

/*
// Desarrolle un programa que pida el nombre, el apellido y la edad de una persona,
// los almacene en una estructura y luego muestre los tres datos en una misma línea, separados por comas
*/

class Practica4_1
{
    struct Persona
    {
        public string Nombre;
        public string Apellido;
        public int Edad;
        public void Mostrar(string x, string y, int z)
        {
            Nombre = x;
            Console.WriteLine(x);
            Apellido = y;
            Console.WriteLine(y);
            Edad = z;
            Console.WriteLine(z);
        }
    }

    static void Main()
    {
        // Console.Write("¿Cuántas personas ingresará al sistema? ");
        // int cont = int.Parse(Console.ReadLine());

        // for (int i = 0; i < cont; i++)
        // {
        Console.WriteLine("Ingrese los siguientes datos de la persona");
        Console.Write("Nombre: ");
        string x = Console.ReadLine();
        Console.Write("Apellido: ");
        string y = Console.ReadLine();
        Console.Write("Edad: ");
        int z = int.Parse(Console.ReadLine());
        // }
        Persona humano = new Persona();
        
        Console.WriteLine("aEl nuevo registro es");
        humano.Mostrar(x, y, z);
    }
}