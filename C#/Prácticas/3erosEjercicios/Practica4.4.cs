using System;

/*
// 
// Elabore una aplicación que pida datos de 8 personas:
// - Nombre
// - Fecha de nacimiento (día, mes, año)
//
// Después deberá repetir lo siguiente:
// - Preguntar un número de mes 
// - Mostrar en pantalla los datos de las personas que cumplan
//   los años durante ese mes.
// Terminará de reetirse cuando se teclee 0 como número de mes.
// 
*/

class Practica4_4
{
    struct Persona
    {
        public string Nombre;
        public int Dia;
        public int Mes;
        public int Anio;
    }
    static void Main()
    {
        int cont = 8;
        Console.WriteLine("Ingrese los datos de las personas");
        Persona[] Registro = new Persona[cont];
        for (int i = 0; i < cont; i++)
        {
            Console.Write("\nNombre: ");
            Registro[i].Nombre = Console.ReadLine();
            Console.Write("Dia de nacimiento: ");
            Registro[i].Dia = int.Parse(Console.ReadLine());
            Console.Write("Mes de nacimiento: ");
            Registro[i].Mes = int.Parse(Console.ReadLine());
            Console.Write("Año de nacimiento: ");
            Registro[i].Anio = int.Parse(Console.ReadLine());
        }

        int menu;
        //string [] bloque = new string[];
        string [] meses = new string[12]{
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"};

        do
        {
            System.Console.WriteLine("Dime un número de mes para saber quién cumplió años ese mes (presione 0 para salir)");
            System.Console.Write("> ");
            menu = int.Parse(Console.ReadLine());

            if (menu == 0)
            {
                break;
            }

            switch (menu)
            {
                case 1:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break;
                case 2:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 3:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 4:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 5:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 6:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 7:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 8:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 9:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 10:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 11:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break; 
                case 12:
                    System.Console.WriteLine("Las personas que cumplieron años en el mes de {0}", meses[menu - 1]);
                    for (int i = 0; i < cont; i++)
                    {
                        if (Registro[i].Mes == menu)
                        {
                            Console.Write(Registro[i].Nombre + "\t\t\t");
                            Console.Write(Registro[i].Dia + "/");
                            Console.Write(meses[menu - 1] + "/");
                            Console.WriteLine(Registro[i].Anio);
                        }
                    }
                    Console.WriteLine();
                    break;
                default:
                    System.Console.WriteLine("Elige un número dentro del rango 1 - 12");
                    break;
            }
            
        } while (menu != 0);

        // Console.ReadKey();
    }
}