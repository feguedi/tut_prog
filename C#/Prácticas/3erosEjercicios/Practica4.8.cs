using System;

/*
// Dadas las estructuras Propietario y Vehículo, escriba un programa que realice lo siguiente: 
// - Permita ingresar los datos de un número indeterminado de Personas y los vehículos que poseen
// - Imprimir información de un propietario en particular y de su vehículo(s) correspondiente
*/

class Practica4_8
{

    struct Vehiculo
    {
        string marca;
        string modelo;
        string clase;
        string color;
        int anio;
        string placa;
        string num_motor;
        string chasis;
    }

    struct Propietario
    {
        string nombre;
        string apellido;
        string direccion;
        Vehiculo carromio;
    }

    static void Main()
    {
        int menu = 0;
        do
        {

            Console.Write("Ingresar nuevo propietario: ");
            

            Console.Write("¿Quiere seguir ingresando datos?(0 para seguir/1 para salir)\n > ");
            menu = int.Parse(Console.ReadLine());
        } while (menu == 0);


    }
}