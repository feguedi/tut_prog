using System;

/*
// Una librería desea controlar su inventario de libros. Crear un programa
// que cree un array de estructuras, que permita almacenar los datos de los
// libros de la librería. El programa debe permitir al usuario introducir todos
// los libros que desee, preguntando si se desea continuar ingresando más libros.
// El programa debe imprimir los datos de cada uno de los libros ingresados y
// sumar la cantidad total de libros existentes. Para ello, los datos que deben
// almacenarse de cada libro deben ser:
// - Título
// - Autor
// - ISBN
// - Precio
// - Cantidad
// - Editorial
// 
// Ejemplo:
// 
// struct tipoPersona {
//        public string nombre;
//        public char inicial;
//        public int edad;
//        public float nota;    
// }
// static void Main(){
//  tipoPersona[] persona = new tipoPersona[1000];
//  persona[0].nombre = "Juan";
//  persona[0].inicial = "J";
//  persona[0].edad = 20;
//  persona[0].nota = 7.5f;
// }
*/

class Practica4_3
{
    struct Libro
    {
        public string Titulo;
        public string Autor;
        public int ISBN;
        public int Precio;
        public int Stock;
        public string Editorial;
        public Redimensionar(a, b)
        {
            Array.Resize(ref a, b);
        }
    }

    static void Main()
    {
        Console.Write("¿Cuántos libros desea registrar? ");
        int cont = int.Parse(Console.ReadLine());
        Libro[] Registro = new Libro[cont];
        for (int i = 0; i < cont; i++)
        {
            Console.Write("Título: ")
            Libro[i].Titulo = Console.ReadLine();
            Console.Write("Autor: ")
            Libro[i].Autor = Console.ReadLine();
            Console.Write("ISBN: ")
            Libro[i].ISBN = int.Parse(Console.ReadLine());
            Console.Write("Precio: ")
            Libro[i].Parse = int.Parse(Console.ReadLine());
            Console.Write("Cantidad en stock: ")
            Libro[i].Stock = int.Parse(Console.ReadLine());
            Console.Write("Editorial: ")
            Libro[i].Editorial = Console.ReadLine();
            if (i >= cont)
            {
                Console.Write("");
            }
        }
    }
}