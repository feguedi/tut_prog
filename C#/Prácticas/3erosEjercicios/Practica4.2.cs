using System;

/*
// La información de todos los empleados de la empresa Artex
// está almacenada en una variable de tipo estructura llamada "empleado".
// La información con que se cuenta de cada empleado es: 
// - Nombre
// - Sexo
// - Sueldo
// Realizar una aplicación que lea un array de estructuras los datos de
// los n empleados de la empresa y que imprima los datos del emplado con salario mayor.
*/

class Practica4_2
{
    struct Empleados
    {
        public string Nombre;
        public char Sexo;
        public int Sueldo;
        public void Mostrar(int x)
        {
            for (int i = 0; i < x; i++)
            {
                Console.WriteLine("Empleado " + i);
                Console.WriteLine(Nombre);
                Console.WriteLine(Sexo);
                Console.WriteLine(Sueldo + "\n");
            }
        }
    }

    static void Main()
    {
        Console.WriteLine("ARTEX\n\n");

        Console.Write("¿Cuántos empleados desea ingresar al sistema? ");
        int cont = int.Parse(Console.ReadLine());
        Empleados[] empleado = new Empleados[cont];
        Console.WriteLine("\nIngrese los datos de los empleados");
        for (int i = 0; i < cont; i++)
        {
            Console.Write("\nNombre: ");
            empleado[i].Nombre = Console.ReadLine();
            Console.Write("Sexo(h/H/m/M): ");
            empleado[i].Sexo = (char) Console.Read();
            Console.Write("\nSueldo: ");
            empleado[i].Sueldo = int.Parse(Console.ReadLine());
        }

        System.Console.WriteLine("\n\nRegistro completo");
        for (int i = 0; i < cont; i++)
        {
            Console.Write("\n" + empleado[i].Nombre);
            Console.Write("\t\t" + empleado[i].Sexo);
            Console.Write("\t\t" + empleado[i].Sueldo);
        }

        //Console.ReadKey();
    }
}