﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ejercicio13
{
    public partial class frm_1 : Form
    {
        public int ls = 0;
        public int lq = 0;
        public int lss = 0;
        public int a = 0;
        public int b = 0;
        public int c = 0;
        public TimeSpan[] ent = new TimeSpan[100];
        public TimeSpan[] ser = new TimeSpan[100];
        public TimeSpan[] sal = new TimeSpan[100];
        public string[,] arEntradas = new String[100, 2];
        public string[,] arServidor = new String[100, 2];
        public string[,] arSalidas = new String[100, 2];
        public StreamWriter writerEn = File.AppendText("DataEn.txt");
        public StreamWriter writerSe = File.AppendText("DataSe.txt");
        public StreamWriter writerSa = File.AppendText("DataSa.txt");

        public frm_1()
        {
            InitializeComponent();
            btn_servidor.Enabled = false;
            btn_salida.Enabled = false;    
           
        }

        private void btn_servidor_Click(object sender, EventArgs e)
        {
             ser[b]= DateTime.Now.TimeOfDay;
            lq--;
            lss++;
            this.lbl_Lq.Text = Convert.ToString(lq);
            this.lbl_Lss.Text = Convert.ToString(lss);
            if (lq == 0)
            {
                btn_servidor.Enabled = false;
                btn_salida.Enabled = true;
            } 
            else
            {
                btn_servidor.Enabled = true;
                btn_salida.Enabled = true;
            }
            arServidor[b, 0] = DateTime.Today.ToShortDateString();
            arServidor[b, 1] = DateTime.Now.TimeOfDay.ToString();
            writerSe.WriteLine(arServidor[b, 0] + "\t" + arServidor[b, 1]);

            b++;
            
        }

        private void frm_1_Load(object sender, EventArgs e)
        {
            

        }


        private void btn_entrada_Click(object sender, EventArgs e)
        {
            ls++;
            lq++;
            ent[a] = DateTime.Now.TimeOfDay;
            this.lbl_Ls.Text = Convert.ToString(ls);
            this.lbl_Lq.Text = Convert.ToString(lq);
            if (ls == 0)
            {
                btn_servidor.Enabled = false;
                btn_salida.Enabled = false;
            }
            else
            {
                btn_servidor.Enabled = true;
            }
            arEntradas[a, 0] = DateTime.Today.ToShortDateString();
            arEntradas[a, 1] = DateTime.Now.TimeOfDay.ToString();
            writerEn.WriteLine(arEntradas[a, 0] + "\t" + arEntradas[a, 1]);

            a++;
        }

        private void btn_salida_Click(object sender, EventArgs e)
        {
            ls--;
            lss--;
            sal[c] = DateTime.Now.TimeOfDay;
            
            this.lbl_Ls.Text = Convert.ToString(ls);
            this.lbl_Lss.Text = Convert.ToString(lss);
            if (ls == 0)
            {
                btn_servidor.Enabled = false;
                btn_salida.Enabled = false;
            }
            else if (lq == 0)
            {
                btn_servidor.Enabled = false;
            }
            else if (lss == 0)
            {
                btn_salida.Enabled = false;
            }
            else
            {
                btn_servidor.Enabled = true;
            }
            TimeSpan ws = sal[c] - ent[c];
            this.lbl_ws.Text = "Ws = " + Convert.ToString(ws);
            TimeSpan wq = ser[c] - ent[c];
            this.lbl_wq.Text = "Wq = " + Convert.ToString(wq);
            TimeSpan wss = sal[c] - ser[c];
            this.lbl_wss.Text = "Wss = " + Convert.ToString(wss);
            arSalidas[c, 0] = DateTime.Today.ToShortDateString();
            arSalidas[c, 1] = DateTime.Now.TimeOfDay.ToString();
            writerSa.WriteLine(arSalidas[c, 0] + "\t" + arSalidas[c, 1]);

            c++;

            //Console.ReadKey();
        }
        private void frm_1_FormClosed(object sender, FormClosedEventArgs e)
        {
            writerEn.Close();
            writerSe.Close();
            writerSa.Close();
        }
    }
}
