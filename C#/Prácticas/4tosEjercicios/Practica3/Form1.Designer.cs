﻿namespace ejercicio13
{
    partial class frm_1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_1));
            this.btn_entrada = new System.Windows.Forms.Button();
            this.btn_servidor = new System.Windows.Forms.Button();
            this.btn_salida = new System.Windows.Forms.Button();
            this.lbl_Ls = new System.Windows.Forms.Label();
            this.lbl_Lq = new System.Windows.Forms.Label();
            this.lbl_Lss = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_ws = new System.Windows.Forms.Label();
            this.lbl_wq = new System.Windows.Forms.Label();
            this.lbl_wss = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_entrada
            // 
            this.btn_entrada.Location = new System.Drawing.Point(51, 176);
            this.btn_entrada.Name = "btn_entrada";
            this.btn_entrada.Size = new System.Drawing.Size(75, 23);
            this.btn_entrada.TabIndex = 0;
            this.btn_entrada.Text = "Entrada";
            this.btn_entrada.UseVisualStyleBackColor = true;
            this.btn_entrada.Click += new System.EventHandler(this.btn_entrada_Click);
            // 
            // btn_servidor
            // 
            this.btn_servidor.Location = new System.Drawing.Point(235, 176);
            this.btn_servidor.Name = "btn_servidor";
            this.btn_servidor.Size = new System.Drawing.Size(75, 23);
            this.btn_servidor.TabIndex = 1;
            this.btn_servidor.Text = "Servidor";
            this.btn_servidor.UseVisualStyleBackColor = true;
            this.btn_servidor.Click += new System.EventHandler(this.btn_servidor_Click);
            // 
            // btn_salida
            // 
            this.btn_salida.Location = new System.Drawing.Point(396, 176);
            this.btn_salida.Name = "btn_salida";
            this.btn_salida.Size = new System.Drawing.Size(75, 23);
            this.btn_salida.TabIndex = 2;
            this.btn_salida.Text = "Salida";
            this.btn_salida.UseVisualStyleBackColor = true;
            this.btn_salida.Click += new System.EventHandler(this.btn_salida_Click);
            // 
            // lbl_Ls
            // 
            this.lbl_Ls.AutoSize = true;
            this.lbl_Ls.Location = new System.Drawing.Point(94, 60);
            this.lbl_Ls.Name = "lbl_Ls";
            this.lbl_Ls.Size = new System.Drawing.Size(16, 13);
            this.lbl_Ls.TabIndex = 3;
            this.lbl_Ls.Text = " 0";
            // 
            // lbl_Lq
            // 
            this.lbl_Lq.AutoSize = true;
            this.lbl_Lq.Location = new System.Drawing.Point(277, 60);
            this.lbl_Lq.Name = "lbl_Lq";
            this.lbl_Lq.Size = new System.Drawing.Size(16, 13);
            this.lbl_Lq.TabIndex = 4;
            this.lbl_Lq.Text = " 0";
            // 
            // lbl_Lss
            // 
            this.lbl_Lss.AutoSize = true;
            this.lbl_Lss.Location = new System.Drawing.Point(436, 60);
            this.lbl_Lss.Name = "lbl_Lss";
            this.lbl_Lss.Size = new System.Drawing.Size(13, 13);
            this.lbl_Lss.TabIndex = 5;
            this.lbl_Lss.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ls =";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(243, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Lq =";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(398, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Lss =";
            // 
            // lbl_ws
            // 
            this.lbl_ws.AutoSize = true;
            this.lbl_ws.Location = new System.Drawing.Point(48, 93);
            this.lbl_ws.Name = "lbl_ws";
            this.lbl_ws.Size = new System.Drawing.Size(29, 13);
            this.lbl_ws.TabIndex = 10;
            this.lbl_ws.Text = "ws =";
            // 
            // lbl_wq
            // 
            this.lbl_wq.AutoSize = true;
            this.lbl_wq.Location = new System.Drawing.Point(232, 93);
            this.lbl_wq.Name = "lbl_wq";
            this.lbl_wq.Size = new System.Drawing.Size(30, 13);
            this.lbl_wq.TabIndex = 11;
            this.lbl_wq.Text = "wq =";
            // 
            // lbl_wss
            // 
            this.lbl_wss.AutoSize = true;
            this.lbl_wss.Location = new System.Drawing.Point(393, 93);
            this.lbl_wss.Name = "lbl_wss";
            this.lbl_wss.Size = new System.Drawing.Size(34, 13);
            this.lbl_wss.TabIndex = 12;
            this.lbl_wss.Text = "wss =";
            // 
            // frm_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 261);
            this.Controls.Add(this.lbl_wss);
            this.Controls.Add(this.lbl_wq);
            this.Controls.Add(this.lbl_ws);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Lss);
            this.Controls.Add(this.lbl_Lq);
            this.Controls.Add(this.lbl_Ls);
            this.Controls.Add(this.btn_salida);
            this.Controls.Add(this.btn_servidor);
            this.Controls.Add(this.btn_entrada);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_1";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_1_FormClosed);
            this.Load += new System.EventHandler(this.frm_1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_entrada;
        private System.Windows.Forms.Button btn_servidor;
        private System.Windows.Forms.Button btn_salida;
        private System.Windows.Forms.Label lbl_Ls;
        private System.Windows.Forms.Label lbl_Lq;
        private System.Windows.Forms.Label lbl_Lss;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_ws;
        private System.Windows.Forms.Label lbl_wq;
        private System.Windows.Forms.Label lbl_wss;
    }
}

