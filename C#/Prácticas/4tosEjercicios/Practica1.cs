using System;
using System.Drawing;
using System.Windows.Forms;

public class Practica_1 : Form
{
    public int val_ls = 0;
    public int val_lq = 0;
    public int val_lss = 0;

    private Button btn_Ls;
    private Button btn_Lq;
    private Button btn_Lss;
    private Label lbl_Lq;
    private Label lbl_Ls;
    private Label lbl_Lss;

    static public void Main ()
    {
        Application.Run (new Practica_1());
    }

    public Practica_1 ()
    {
        this.btn_Ls = new System.Windows.Forms.Button();
        this.btn_Lq = new System.Windows.Forms.Button();
        this.btn_Lss = new System.Windows.Forms.Button();
        this.lbl_Lq = new System.Windows.Forms.Label();
        this.lbl_Ls = new System.Windows.Forms.Label();
        this.lbl_Lss = new System.Windows.Forms.Label();

        this.btn_Ls.Click += new EventHandler (Btn_Ls_Clicked);
        this.btn_Lq.Click += new EventHandler (Btn_Lq_Clicked);
        this.btn_Lss.Click += new EventHandler (Btn_Lss_Clicked);

        SuspendLayout();

        this.btn_Ls.Location = new System.Drawing.Point(51, 176);
        this.btn_Ls.Name = "btn_Ls";
        this.btn_Ls.Size = new System.Drawing.Size(75, 23);
        this.btn_Ls.TabIndex = 0;
        this.btn_Ls.Enabled = true;
        this.btn_Ls.Text = "Entrada";
        this.btn_Ls.UseVisualStyleBackColor = true;

        this.btn_Lq.Location = new System.Drawing.Point(235, 176);
        this.btn_Lq.Name = "btn_Lq";
        this.btn_Lq.Size = new System.Drawing.Size(75, 23);
        this.btn_Lq.TabIndex = 1;
        this.btn_Lq.Enabled = false;
        this.btn_Lq.Text = "Servidor";
        this.btn_Lq.UseVisualStyleBackColor = true;

        this.btn_Lss.Location = new System.Drawing.Point(396, 176);
        this.btn_Lss.Name = "btn_Lss";
        this.btn_Lss.Size = new System.Drawing.Size(75, 23);
        this.btn_Lss.TabIndex = 2;
        this.btn_Lss.Enabled = false;
        this.btn_Lss.Text = "Salida";
        this.btn_Lss.UseVisualStyleBackColor = true;

        this.lbl_Ls.AutoSize = true;
        this.lbl_Ls.Location = new System.Drawing.Point(51, 60);
        this.lbl_Ls.Name = "lbl_Ls";
        this.lbl_Ls.Size = new System.Drawing.Size(36, 13);
        this.lbl_Ls.TabIndex = 3;
        this.lbl_Ls.Text = "Ls = " + val_ls;

        this.lbl_Lq.AutoSize = true;
        this.lbl_Lq.Location = new System.Drawing.Point(232, 60);
        this.lbl_Lq.Name = "lbl_Lq";
        this.lbl_Lq.Size = new System.Drawing.Size(37, 13);
        this.lbl_Lq.TabIndex = 4;
        this.lbl_Lq.Text = "Lq = " + val_lq;

        this.lbl_Lss.AutoSize = true;
        this.lbl_Lss.Location = new System.Drawing.Point(393, 60);
        this.lbl_Lss.Name = "lbl_Lss";
        this.lbl_Lss.Size = new System.Drawing.Size(41, 13);
        this.lbl_Lss.TabIndex = 5;
        this.lbl_Lss.Text = "Lss = " + val_lss;

        Controls.Add(this.lbl_Lss);
        Controls.Add(this.lbl_Lq);
        Controls.Add(this.lbl_Ls);
        Controls.Add(this.btn_Ls);
        Controls.Add(this.btn_Lq);
        Controls.Add(this.btn_Lss);

        AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        ClientSize = new System.Drawing.Size(533, 261);
        Icon = new Icon("iconito.ico");
        Name = "frm_1";
        Text = "Main";
        ResumeLayout(false);
        CenterToScreen();
        PerformLayout();
    }

    private void Btn_Ls_Clicked (object sender, EventArgs e)
    {
        // System.Windows.Forms.MessageBox.Show("Entrando un nuevo dato");
        val_ls++;

        // lbl_Lq.Text = "Lq = " + val_lq;
        lbl_Ls.Text = "Ls = " + val_ls;
        btn_Lq.Enabled = true;
        btn_Lss.Enabled = true;
    }

    private void Btn_Lq_Clicked (object sender, EventArgs e)
    {
        // System.Windows.Forms.MessageBox.Show("Un dato en el Servidor");
        if (val_ls >= 1)
        {
            val_lq = 1;
            lbl_Lq.Text = "Lq = " + val_lq;

            val_ls--;
            lbl_Ls.Text = "Ls = " + val_ls;

            if (val_ls == 1)
            {
                btn_Lq.Enabled = false;

                if (val_lq == 0)
                {
                    btn_Lss.Enabled = false;
                }
            }
        }
    }

    private void Btn_Lss_Clicked (object sender, EventArgs e)
    {
        // System.Windows.Forms.MessageBox.Show("Salida de un dato");
        if (val_lq == 0 && val_ls >= 1)
        {
            val_ls--;
            lbl_Ls.Text = "Ls = " + val_ls;
            val_lss++;
            lbl_Lss.Text = "Lss = " + val_lss;            
        }
        if (val_lq >= 1)
        {
            val_lq--;
            lbl_Lq.Text = "Lq = " + val_lq;
            val_lss++;
            lbl_Lss.Text = "Lss = " + val_lss;
        }

        if (val_ls == 0 && val_lq == 0)
        {
            btn_Lq.Enabled = false;
            btn_Lss.Enabled = false;
        }
    }
}