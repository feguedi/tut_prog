using System;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;

public class Practica_2 : Form
{
    public DateTime horaLocal = DateTime.UtcNow;

    private static TimeSpan tempo1;
    private static TimeSpan tempo2;
    private static TimeSpan tempo3;

    public int contEn = 0;
    public int contSe = 0;
    public int contSa = 0;

    public int val_ls = 0;
    public int val_lq = 0;
    public int val_lss = 0;

    public string[][] hora_ws = new string[1][];
    public string[][] hora_wq = new string[1][];
    public string[][] hora_wss = new string[1][];

    private Button btn_Entrada;
    private Button btn_Servidor;
    private Button btn_Salida;
    private Label lbl_Lq;
    private Label lbl_Ls;
    private Label lbl_Lss;
    private Label lbl_Wq;
    private Label lbl_Ws;
    private Label lbl_Wss;

    static void Main ()
    {
        Application.Run (new Practica_2());
    }

    public Practica_2 ()
    {
        this.btn_Entrada = new System.Windows.Forms.Button();
        this.btn_Servidor = new System.Windows.Forms.Button();
        this.btn_Salida = new System.Windows.Forms.Button();
        this.lbl_Lq = new System.Windows.Forms.Label();
        this.lbl_Ls = new System.Windows.Forms.Label();
        this.lbl_Lss = new System.Windows.Forms.Label();
        this.lbl_Wq = new System.Windows.Forms.Label();
        this.lbl_Ws = new System.Windows.Forms.Label();
        this.lbl_Wss = new System.Windows.Forms.Label();

        this.btn_Entrada.Click += new EventHandler (btn_Entrada_Clicked);
        this.btn_Servidor.Click += new EventHandler (btn_Servidor_Clicked);
        this.btn_Salida.Click += new EventHandler (btn_Salida_Clicked);

        SuspendLayout();

        this.btn_Entrada.Location = new System.Drawing.Point(51, 176);
        this.btn_Entrada.Name = "btn_Entrada";
        this.btn_Entrada.Size = new System.Drawing.Size(75, 23);
        this.btn_Entrada.TabIndex = 0;
        this.btn_Entrada.Enabled = true;
        this.btn_Entrada.Text = "Entrada";
        this.btn_Entrada.UseVisualStyleBackColor = true;

        this.btn_Servidor.Location = new System.Drawing.Point(235, 176);
        this.btn_Servidor.Name = "btn_Servidor";
        this.btn_Servidor.Size = new System.Drawing.Size(75, 23);
        this.btn_Servidor.TabIndex = 1;
        this.btn_Servidor.Enabled = false;
        this.btn_Servidor.Text = "Servidor";
        this.btn_Servidor.UseVisualStyleBackColor = true;

        this.btn_Salida.Location = new System.Drawing.Point(396, 176);
        this.btn_Salida.Name = "btn_Salida";
        this.btn_Salida.Size = new System.Drawing.Size(75, 23);
        this.btn_Salida.TabIndex = 2;
        this.btn_Salida.Enabled = false;
        this.btn_Salida.Text = "Salida";
        this.btn_Salida.UseVisualStyleBackColor = true;

        this.lbl_Ls.AutoSize = true;
        this.lbl_Ls.Location = new System.Drawing.Point(51, 60);
        this.lbl_Ls.Name = "lbl_Ls";
        this.lbl_Ls.Size = new System.Drawing.Size(36, 13);
        this.lbl_Ls.TabIndex = 3;
        this.lbl_Ls.Text = "Ls = " + val_ls;

        this.lbl_Lq.AutoSize = true;
        this.lbl_Lq.Location = new System.Drawing.Point(232, 60);
        this.lbl_Lq.Name = "lbl_Lq";
        this.lbl_Lq.Size = new System.Drawing.Size(37, 13);
        this.lbl_Lq.TabIndex = 4;
        this.lbl_Lq.Text = "Lq = " + val_lq;

        this.lbl_Lss.AutoSize = true;
        this.lbl_Lss.Location = new System.Drawing.Point(393, 60);
        this.lbl_Lss.Name = "lbl_Lss";
        this.lbl_Lss.Size = new System.Drawing.Size(41, 13);
        this.lbl_Lss.TabIndex = 5;
        this.lbl_Lss.Text = "Lss = " + val_lss;

        this.lbl_Ws.AutoSize = true;
        this.lbl_Ws.Location = new System.Drawing.Point(51, 90);
        this.lbl_Ws.Name = "lbl_Ws";
        this.lbl_Ws.Size = new System.Drawing.Size(36, 13);
        this.lbl_Ws.TabIndex = 3;
        this.lbl_Ws.Text = "Ws = " + hora_ws;
        
        this.lbl_Wq.AutoSize = true;
        this.lbl_Wq.Location = new System.Drawing.Point(232, 90);
        this.lbl_Wq.Name = "lbl_Wq";
        this.lbl_Wq.Size = new System.Drawing.Size(37, 13);
        this.lbl_Wq.TabIndex = 4;
        this.lbl_Wq.Text = "Wq = " + hora_wq;
        
        this.lbl_Wss.AutoSize = true;
        this.lbl_Wss.Location = new System.Drawing.Point(393, 90);
        this.lbl_Wss.Name = "lbl_Wss";
        this.lbl_Wss.Size = new System.Drawing.Size(41, 13);
        this.lbl_Wss.TabIndex = 5;
        this.lbl_Wss.Text = "Wss = " + hora_wss;

        Controls.Add(this.lbl_Lss);
        Controls.Add(this.lbl_Lq);
        Controls.Add(this.lbl_Ls);
        Controls.Add(this.lbl_Wss);
        Controls.Add(this.lbl_Wq);
        Controls.Add(this.lbl_Ws);
        Controls.Add(this.btn_Entrada);
        Controls.Add(this.btn_Servidor);
        Controls.Add(this.btn_Salida);

        AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        ClientSize = new System.Drawing.Size(533, 261);
        Icon = new Icon("iconito.ico");
        Name = "frm_1";
        Text = "Main";
        ResumeLayout(false);
        CenterToScreen();
        PerformLayout();
    }

    private void btn_Entrada_Clicked (object sender, EventArgs e)
    {
        val_ls++;

        lbl_Ls.Text = "Ls = " + val_ls;
        btn_Servidor.Enabled = true;
        btn_Salida.Enabled = true;

        // EventHandler (Hora_Entrada);

        tempo1 = DateTime.Now.TimeOfDay;
        string tmp = tempo1.ToString(@"mm\:ss");
        System.Console.WriteLine("Hora de entrada: " + tmp);
        System.Windows.Forms.MessageBox.Show("Hora de entrada: " + tmp);
        hora_ws[contEn][0] = tmp;
        contEn++;

    }

    private void btn_Servidor_Clicked (object sender, EventArgs e)
    {
        
        if (val_ls >= 1)
        {
            val_lq = 1;
            lbl_Lq.Text = "Lq = " + val_lq;

            val_ls--;
            lbl_Ls.Text = "Ls = " + val_ls;

            if (val_ls == 1)
            {
                btn_Servidor.Enabled = false;

                if (val_lq == 0)
                {
                    btn_Salida.Enabled = false;
                }
            }
        }


    }

    private void btn_Salida_Clicked (object sender, EventArgs e)
    {
        if (val_lq == 0 && val_ls >= 1)
        {
            val_ls--;
            lbl_Ls.Text = "Ls = " + val_ls;
            val_lss++;
            lbl_Lss.Text = "Lss = " + val_lss;            
        }
        if (val_lq >= 1)
        {
            val_lq--;
            lbl_Lq.Text = "Lq = " + val_lq;
            val_lss++;
            lbl_Lss.Text = "Lss = " + val_lss;
        }

        if (val_ls == 0 && val_lq == 0)
        {
            btn_Servidor.Enabled = false;
            btn_Salida.Enabled = false;
        }


    }

}