using System;
using System.Diagnostics;
using System.IO;

class DatosPersonales
{
    struct Persona
    {
        public string nombre;
        public string direccion;
        public int edad;
    }

    static StreamWriter Escribir;
    static StreamReader Leer;

    static void Main()
    {
        Console.Title = "Datos Personales";
        int a = 0;
        int contador = 0;

        string opcion;
        string linea;

        Persona Acceso = new Persona();

        do
        {
            Console.Clear();
            Escribir = new StreamWriter("DatosPersonales.txt", true);
            System.Console.WriteLine("Persona con ID: [{0}]", (a + 1));
            
            System.Console.Write("Nombre: ");
            Acceso.nombre = Console.ReadLine();

            System.Console.Write("Edad: ");
            Acceso.edad = int.Parse(Console.ReadLine());
            
            System.Console.Write("Dirección: ");
            Acceso.direccion = Console.ReadLine();

            Escribir.WriteLine("Persona con ID: " + (a + 1));
            Escribir.WriteLine("Nombre: " + Acceso.nombre);
            Escribir.WriteLine("Edad: " + Acceso.edad);
            Escribir.WriteLine("Dirección: " + Acceso.direccion);
            Escribir.Close();
            a++;

            System.Console.WriteLine("------------------------");

            System.Console.Write("¿Desea ingresar otro registro? [S/N] ");
            opcion = Console.ReadLine();

            if (opcion == "N" || opcion == "n")
            {
                Process.Start("DatosPersonales.txt");
            }

        } while (opcion == "S" || opcion == "s");

        Leer = new StreamReader("DatosPersonales.txt");
        linea = Leer.ReadLine();
        while (linea != null)
        {
            System.Console.WriteLine(linea);
        }
        Leer.Close();
        //Console.ReadKey();
    }
}