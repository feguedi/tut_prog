using System;

/*
*  Tome en cuenta los siguientes arreglos 
*       1, 2, 6, 4, 2, 6, 9 
*       Azul, rojo, amarillo, verde, azul 
*       2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0 
*  Copie el contenido del arreglo a al arreglo d
*/

public class Practica2_5
{
    static public void Main()
    {
        int[] arrA = new int[7]{1, 2, 6, 4, 2, 6, 9};
        string[] arrB = new string[5]{"azul", "rojo", "amarillo", "verde", "azul"};
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};
        int[] arrD = new int[7];

        Array.Copy(arrA, arrD, 7);
        System.Console.WriteLine("El nuevo arreglo es:");
        for (int i = 0; i < 7; i++)
        {
            if (i < 6)
            {
                System.Console.Write("{0}, ", arrD[i]);
            }
            else
            {
                System.Console.WriteLine(arrD[i]);   
            }
        }
    }
}