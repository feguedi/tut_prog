using System;

/*
// Unir los arreglos "a" "b" "c" "d" y "e" en un nuevo arreglo "f"
*/

public class Practica2_7
{
    static public void Main()
    {
        int[] arrA = new int[7]{1, 2, 6, 4, 2, 6, 9};
        string[] arrB = new string[5]{"azul", "rojo", "amarillo", "verde", "azul"};
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};
        int[] arrD = new int[7];

        Array.Copy(arrA, arrD, 7);

        double[] arrE = new double[14];
        for (int i = 0; i < 14; i++)
        {
            if (i < 7)
            {
                arrE[i] = arrA[i];
            }
            if (i >= 7)
            {
                arrE[i] = arrC[i - 7];
            }
        }

        string[] arrF = new string[arrA.Length + arrB.Length + arrC.Length + arrD.Length + arrE.Length];
        for (int i = 0; i < (arrA.Length + arrB.Length + arrC.Length + arrD.Length + arrE.Length); i++)
        {
            if (i < 7)
            {
                arrF[i] = Convert.ToString(arrA[i]);
                if (i < 6)
                {
                    System.Console.Write("{0}\t",arrF[i]);
                }
                else
                {
                    System.Console.WriteLine(arrF[i]);
                }
            }
            if (i >= 7 && i < 12)
            {
                arrF[i] = arrB[i - 7];
                if (i < 11)
                {
                    System.Console.Write("{0}\t",arrF[i]);
                }
                else
                {
                    System.Console.WriteLine(arrF[i]);
                }
            }
            if (i >= 12 && i < 19)
            {
                arrF[i] = Convert.ToString(arrC[i - 12]);
                if (i < 18)
                {
                    System.Console.Write("{0}\t",arrF[i]);
                }
                else
                {
                    System.Console.WriteLine(arrF[i]);
                }
            }
            if (i >= 19 && i < 26)
            {
                arrF[i] = Convert.ToString(arrD[i - 19]);
                if (i < 25)
                {
                    System.Console.Write("{0}\t",arrF[i]);
                }
                else
                {
                    System.Console.WriteLine(arrF[i]);
                }
            }
            if (i >= 26 && i < 40)
            {
                arrF[i] = Convert.ToString(arrE[i - 26]);
                if (i < 39)
                {
                    System.Console.Write("{0}\t",arrF[i]);
                }
                else
                {
                    System.Console.WriteLine(arrF[i]);
                }
            }
        }
    }
}