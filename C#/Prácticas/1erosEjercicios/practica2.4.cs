using System;

/*
* Modifique el problema anterior usando los métodos de arreglo necesarios.
* Elabora una aplicación que pida al usuario 5 lecturas de temperaturas mostrando en pantalla cada una de las lecturas en orden inverso en el cual fueron capturadas (sin usar ningún método de arreglos).
*/

public class Practica2_4
{
    static public void Main()
    {
        int[] sol = new int[5];
        for (int i = 0; i < 5; i++)
        {
            System.Console.Write("Agregue un número: ");
            sol[i] = int.Parse(Console.ReadLine());
        }

        Array.Reverse(sol);
        System.Console.Write("El arreglo es: ");
        for (int i = 0; i < 5; i++)
        {
            if (i < 4)
            {
                System.Console.Write("{0}, ", sol[i]);
            }
            else
            {
                System.Console.WriteLine(sol[i]);
            }
            
        }

        // System.Console.ReadKey();
    }
}