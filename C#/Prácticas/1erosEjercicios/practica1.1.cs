using System;

public class Practica1
{
    static public void Main()
    {
        int[] temperaturas = new int[7];
        int min = 0;
        int max = 0;
        double prom = 0.0;

        /*Espacio para pedir números*/
 
        for (int i = 0; i < temperaturas.Length; i++)
        {
            if (temperaturas[i] > max)
            {
                max = temperaturas[i];
            }
            if (min == 0)
            {
                min = temperaturas[i];
            }
            else 
            {
                if (temperaturas[i] < min)
                {
                    min = temperaturas[i];
                }
            }
            prom += temperaturas[i];
        }
        
        prom = prom / 7;
        System.Console.WriteLine("La temperatura en promedio registrada es: " + prom);
        System.Console.WriteLine("La temperatura mínima registrada es: " + min);
        System.Console.WriteLine("La temperatura máxima registrada es: " + max);
        System.Console.ReadKey();   
    }
}