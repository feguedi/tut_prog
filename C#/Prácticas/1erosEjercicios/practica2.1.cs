using System;

/*
* Suponga que tiene el siguiente arreglo en números: [1, 2, 1, 1, 2]. Desarrolle una aplicación que calcule cuántos "2" aparecen dentro del arreglo
*/

public class Practica2_1
{
    static public void Main()
    {
        int[] arreglo = new int[]{1, 2, 1, 1, 2};
        int calc = 0;
        foreach (int i in arreglo)
        {
            if(i == 2)
            {
                calc++;
            }
        }
        System.Console.WriteLine("El arreglo es el siguiente: ");
        for (int i = 0; i < arreglo.Length; i++)
        {
            System.Console.WriteLine(arreglo[i]);
        }
        System.Console.WriteLine("Hay {0} números 2 en el arreglo", calc);
    }
}