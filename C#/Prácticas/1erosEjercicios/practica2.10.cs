using System;

/*
// 
// Ordene los elementos del arreglo "c" en orden descendente
// 
*/

public class Practica2_10
{
    static public void Main()
    {
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};
        System.Console.WriteLine("El arreglo es:");
        for (int i = 0; i < 7; i++)
        {
            System.Console.WriteLine(" - " + arrC[i]);
        }
        
        System.Console.WriteLine("\nQue ordenado descendentemente es:");
        Array.Sort(arrC, 0, arrC.Length);
        for (int i = 0; i < 7; i++)
        {
            System.Console.WriteLine(" - " + arrC[i]);
        }
    }
}