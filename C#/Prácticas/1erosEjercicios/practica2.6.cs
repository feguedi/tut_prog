using System;

/*
// Unir los arreglos "a" y "c" en el arreglo "e"
*/

public class Practica2_6
{
    static public void Main()
    {
        int[] arrA = new int[7]{1, 2, 6, 4, 2, 6, 9};
        //string[] arrB = new string[5]{"azul", "rojo", "amarillo", "verde", "azul"};
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};

        double[,] arrE = new double[2,7];
        System.Console.WriteLine("El arreglo 'E' es:");
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (i == 0)
                {
                    arrE[i,j] = arrA[j];
                }
                else
                {
                    arrE[i,j] = arrC[j];
                }
                if (j != 6)
                {
                    System.Console.Write("{0}, \t",arrE[i,j]);
                }
                else
                {
                    System.Console.WriteLine("{0}",arrE[i,j]);
                }
            }
        }
    }
}