using System;

/*
* Elabora una aplicación que pida al usuario 5 lecturas de temperaturas mostrando en pantalla cada una de las lecturas en orden inverso en el cual fueron capturadas (sin usar ningún método de arreglos).
*/

public class Practica2_3
{
    static public void Main()
    {
        
        int[] sol = new int[5];
        for (int i = 0; i < 5; i++)
        {
            System.Console.Write("Agregue un número: ");
            sol[i] = int.Parse(Console.ReadLine());
        }
        
        for (int i = 4; i >= 0; i--)
        {
            System.Console.Write("Posición ", i, " dentro del arreglo: ");
            System.Console.WriteLine(sol[i]);
        }
        System.Console.ReadKey();
    }
}