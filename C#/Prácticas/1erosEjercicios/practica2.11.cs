using System;

/*
// 
// Ordene los elementos del arreglo a de forma ascendente
// 
*/

public class Practica2_11
{
    static public void Main()
    {
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};
        System.Console.WriteLine("El arreglo es:");
        for (int i = 0; i < 7; i++)
        {
            System.Console.WriteLine(" - " + arrC[i]);
        }
        
        System.Console.WriteLine("\nQue ordenado ascendentemente es:");
        Array.Sort(arrC, 0, arrC.Length);
        Array.Reverse(arrC)
        for (int i = 0; i < 7; i++)
        {
            System.Console.WriteLine(" - " + arrC[i]);
        }
    }
}