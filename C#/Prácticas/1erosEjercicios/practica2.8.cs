using System;

/*
// 
// Desarrolla una aplicación que permita buscar un valor desde teclado en la lista de valores del arreglo "b"
// y en caso de encontrarlo, devolver la posición del ultimo valor encontrado
// 
*/

public class Practica2_8
{
    static public void Main()
    {
        string[] arrB = new string[5]{"azul", "rojo", "amarillo", "verde", "azul"};
        /*System.Console.WriteLine("Los valores del arreglo 'B' son: ");
        for (int i = 0; i < 4; i++)
        {
            System.Console.WriteLine(" - " + arrB[i]);
        }*/

        System.Console.Write("Ingrese el color que busca dentro del arreglo: ");
        string bus = Console.ReadLine();
        int enc = 0;
        for (int i = 0; i < 5; i++)
        {
            if (bus == arrB[i])
            {
                enc = i + 1;
            }
        }

        System.Console.WriteLine("El color está en la posición: " + enc);
    }
}