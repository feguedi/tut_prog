using System;

/*
// 
// Tomando en cuenta los arreglos "a", "b", "c", "d", "e" y "f", pedir al usuario que elija uno de ellos y mostrar
// el número de dimensiones y el tamaño del arreglo seleccionado
// 
*/

public class Practica2_12
{
    static public void Main()
    {
        int[] arrA = new int[7]{1, 2, 6, 4, 2, 6, 9};
        string[] arrB = new string[5]{"azul", "rojo", "amarillo", "verde", "azul"};
        double[] arrC = new double[7]{2.2, 7.9, 2.2, 1.6, 5.8, 6.5, 8.0};
        int[] arrD = new int[7];

        Array.Copy(arrA, arrD, 7);

        double[] arrE = new double[14];
        for (int i = 0; i < 14; i++)
        {
            if (i < 7)
            {
                arrE[i] = arrA[i];
            }
            if (i >= 7)
            {
                arrE[i] = arrC[i - 7];
            }
        }

        int tamF = arrA.Length + arrB.Length + arrC.Length + arrD.Length + arrE.Length;
        string[] arrF = new string[tamF];
        for (int i = 0; i < tamF; i++)
        {
            if (i < 7)
            {
                arrF[i] = Convert.ToString(arrA[i]);
            }
            if (i >= 7 && i < 12)
            {
                arrF[i] = arrB[i - 7];
            }
            if (i >= 12 && i < 19)
            {
                arrF[i] = Convert.ToString(arrC[i - 12]);
            }
            if (i >= 19 && i < 26)
            {
                arrF[i] = Convert.ToString(arrD[i - 19]);
            }
            if (i >= 26 && i < 40)
            {
                arrF[i] = Convert.ToString(arrE[i - 26]);
            }
        }

        System.Console.Write("De los arreglos disponibles, elija de cuál desea saber sus dimensiones y tamaño\na)\nb)\nc)\nd)\ne)\nf)\n\n> ");
        string sel = Console.ReadLine();
        switch (sel)
        {
            case "a":
                System.Console.Write("El arreglo 'A' ");
                System.Console.Write("es de tamaño {0} ", arrA.Length);
                System.Console.WriteLine("y de dimensión " + arrA.Rank);
                break;
            case "b":
                System.Console.Write("El arreglo 'B' "); 
                System.Console.Write("es de tamaño {0} ");
                System.Console.WriteLine("y de dimensión " + arrB.Rank);
                break;
            case "c":
                System.Console.Write("El arreglo 'C' ");
                System.Console.Write("es de tamaño {0} ", arrC.Length);
                System.Console.WriteLine("y de dimensión " + arrC.Rank);
                break;
            case "d":
                System.Console.Write("El arreglo 'D' ");
                System.Console.Write("es de tamaño {0} ", arrD.Length);
                System.Console.WriteLine("y de dimensión " + arrD.Rank);
                break;
            case "e":
                System.Console.Write("El arreglo 'E' ");
                System.Console.Write("es de tamaño {0} ", arrE.Length);
                System.Console.WriteLine("y de dimensión " + arrE.Rank);
                break;
            case "f":
                System.Console.Write("El arreglo 'F' ");
                System.Console.Write("es de tamaño {0} ", arrF.Length);
                System.Console.WriteLine("y de dimensión " + arrF.Rank);
                break;
            default:
                System.Console.WriteLine("Selecciona una opción válida");
                break;
        }

        //Console.ReadKey();
    }
}