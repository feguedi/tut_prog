using System;

/*
* Usted tiene el siguiente arreglo: [0, 2, 5, 7, 6], [0, 0, 0, 3, 8], [1, 2, 1, 6, 7]. Elabore un programa que calcule cuántos valores de la matriz son mayores que 0.
*/

public class Practica2_2
{
    static public void Main()
    {
        int[,] arreglo = new int[3, 5]{{0, 2, 5, 7, 6}, {0, 0, 0, 3, 8}, {1, 2, 1, 6, 7}};
        int calc = 0;
        System.Console.WriteLine("El arreglo es: ");
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                if(j < 4)
                {
                    System.Console.Write("{0}, ", arreglo[i,j]);
                }
                else
                {
                    System.Console.WriteLine(arreglo[i,j]);
                }
                if (arreglo[i,j] > 0)
                {
                    calc++;
                }
            }
        }

        System.Console.WriteLine("Hay {0} valores mayores a 0 dentro del arreglo", calc);

    }
}