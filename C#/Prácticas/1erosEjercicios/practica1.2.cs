using System;

public class Practica1
{
    static public void Main()
    {
        int[,] temperaturas = new int[7,24];
        int min = 0;
        int max = 0;
        double prom = 0.0;

        /* 
        // Espacio para meter números
        // for (int i = 0; i < 7; i++)
        // {
        //     temperaturas[i] = int.Parse(Console.ReadLine());
        // }
        */

        Random r = new Random();

        System.Console.WriteLine("Los valores de la semana son: ");
        for (int dia = 0; dia <= 7; dia++)
        {
            for (int hora = 0; hora < 24; hora++)
            {
                temperaturas[dia,hora] = r.Next(20, 30);
                System.Console.WriteLine("Registrando nueva temperatura: {0}. Hora {1} del día {2}",temperaturas[dia,hora], hora, dia + 1);

                if (temperaturas[dia,hora] > max)
                {
                    max = temperaturas[dia,hora];
                }
                if (min == 0)
                {
                    min = temperaturas[dia,hora];
                }
                else 
                {
                    if (temperaturas[dia,hora] < min)
                    {
                        min = temperaturas[dia,hora];
                    }
                }
                prom += temperaturas[dia,hora];
            }
            // System.Console.WriteLine(temperaturas[dia,hora]);
        }
        
        prom = prom / 7;
        System.Console.WriteLine("La temperatura en promedio registrada es: " + prom);
        System.Console.WriteLine("La temperatura mínima registrada es: " + min);
        System.Console.WriteLine("La temperatura máxima registrada es: " + max);
        System.Console.ReadKey();   
    }
}