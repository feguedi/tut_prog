using System;

public class Practica3_1_1
{
    static public void Main()
    {
        int[,] matrA = new int[4,3]{{3,6,9},{12,15,18},{21,24,27},{30,33,36}};
        
        Array.Reverse(matrA);
        
        Console.WriteLine("El nuevo arreglo quedó:");
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Console.Write(matrA[i,j]);
            }
            Console.WriteLine();
        }
    }
}