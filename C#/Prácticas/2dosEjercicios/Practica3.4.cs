using System;

/*

Un departamento de una empresa  tiene 5 jefes de  área. Cada Jefe tiene a su cargo los siguientes empleados:

a.Jefe 1 = 2 empleados con Salarios( 9000,8500) 
b.Jefe 2 = 5 empleados con Salarios( 9000,8500,8600,6000,3000) 
c.Jefe 3 = 3empleados con Salarios( 5000,,800, 8500) 
d.Jefe 4 = 4 empleados con Salarios(3500,9500,9000,8500) 
e.Jefe 5 = 1 empleados con Salario (18000) 

Determinar  cual es el departamento con mas empleados.
En cual departamento  se encuentra el salario mas alto.
En cual departamento  se encuentra el salario mas bajo.
En cual departamento  se gasta mas por concepto de pago de nominas.

*/

public class Practica3_3
{
    static public void Main()
    {
        int[] jefe1 = new int[2]{9000,8500};
        int[] jefe2 = new int[5]{9000,8500,8600,6000,3000};
        int[] jefe3 = new int[3]{5000,800,8500};
        int[] jefe4 = new int[4]{3500,9500,9000,8500};
        int[] jefe5 = new int[1]{18000};
        int[] cont = new int[5];
        
        foreach (int cont in jefe1)
        {
            System.Console.Write(jefe1[cont]);
            cont[0] += 1;
        }
        System.Console.WriteLine();        
        foreach (int cont in jefe2)
        {
            System.Console.Write(jefe2[cont]);
            cont[1] += 1;
        }
        System.Console.WriteLine();
        foreach (int cont in jefe3)
        {
            System.Console.Write(jefe3[cont]);
            cont[2] += 1;
        }
        System.Console.WriteLine();
        foreach (int cont in jefe4)
        {
            System.Console.Write(jefe4[cont]);
            cont[3] += 1;
        }
        System.Console.WriteLine();
        foreach (int cont in jefe5)
        {
            System.Console.Write(jefe5[cont]);
            cont[4] += 1;
        }
        System.Console.WriteLine();


    }
}

//dropbox.com/s/rc8xijfnyvz1kpd/Estructuras.pdf?dl=0