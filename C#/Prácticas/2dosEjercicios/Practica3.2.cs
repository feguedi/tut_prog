using System;

/*
// 
// En un arreglo bidimensional que almacena la cantidad de computadoras vendidas por tres vendedores en cuatro zonas diferentes. Se pide mostrar:
// - La zona que más computadoras vendió
// - El vendedor que menos computadoras vendió
// - La cantidad de computadoras vendidas por todos los vendedores en todas las zonas
// 
*/

public class Practica3_2
{
    static public void Main()
    {
        int[,] venta = new int[3,4];
        int comVent = 0;
        int menVent  = 0;
        Random r = new Random();
        int menor = 0;

        for (int i = 0; i < 3; i++)
        {
            int sum = 0;
            for (int j = 0; j < 4; j++)
            {
                venta[i,j] = r.Next(0,15);
                if (j != 3)
                {
                    Console.Write("{0}, \t", venta[i,j]);
                }
                else
                {
                    Console.WriteLine(venta[i,j]);
                }
                // Mayores ventas de compus
                if (i != 0 && j != 0)
                {
                    if (comVent > venta[i,j])
                    {
                        comVent = venta[i,j];
                    }
                }
                else
                {
                    comVent = venta[i,j];
                }

                sum += venta[i,j];
            }

            // Menores números de vendedores

            if (sum < menor)
            {
                menor = sum;
            }
        }

        int tot = 0;
        foreach (int i in venta)
        {
            tot += i;
        }

        System.Console.WriteLine("Zona que más vendió: " + comVent);
        System.Console.WriteLine("Vendedor que menos vendió: " + menVent);
        System.Console.WriteLine("Total vendido: " + tot);
    }
}