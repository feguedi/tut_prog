using System;

public class Practica3_1_2
{
    static public void Main()
    {
        int[,] matrA = new int[4,3]{{3,6,9},{12,15,18},{21,24,27},{30,33,36}};
        int min = 0;
        int max = 0;
        //int prom;
        int sum = 0;
        int tot = 0;
        int prod = 1;

        foreach (int i in matrA)
        {
            if(i != matrA[0,0])
            {
                if (i > max)
                {
                    max = i;
                }
                if (i < min)
                {
                    min = i;
                }
                tot++;
                sum += i;
                prod *= i;
                System.Console.WriteLine("El producto actual es {0}", prod);
            }
            else
            {
                min = matrA[0,0];
                max = matrA[0,0];
                tot++;
                sum += i;
                prod *= i;
                System.Console.WriteLine("El producto actual es {0}", prod);
            }
        }

        System.Console.WriteLine("El valor mínimo es: " + min);
        System.Console.WriteLine("El valor máximo es: " + max);
        System.Console.WriteLine("El promedio de valores es: {0}", sum / tot);
        System.Console.WriteLine("La suma de todos los valores es: " + sum);
        System.Console.WriteLine("El total de valores es: " + tot);
        System.Console.WriteLine("El producto total es: " + prod);
    }
}
