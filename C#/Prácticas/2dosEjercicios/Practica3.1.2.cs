using System;

public class Practica3_1_2
{
    static public void Main()
    {
        int[,] matrA = new int[4,3]{{3,6,9},{12,15,18},{21,24,27},{30,33,36}};
        int[,] matrB = new int[4,3];

        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < 4; i++)
            {
                foreach (int k in matrA)
                {
                    System.Console.WriteLine("Agregando valor: {0} en la posición {1},{2}", k, i, j);
                    matrB[i,j] = k;

                }
            }
        }

        Console.WriteLine("El nuevo arreglo:");
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (j != 2)
                {
                    Console.Write("{0}, \t", matrB[i,j]);
                }
                else
                {
                    Console.WriteLine(matrB[i,j]);
                }
            }
        }
    }
}