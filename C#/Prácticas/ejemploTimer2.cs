using System;

class Programa
{
    static void Main()
    {
        TimeSpan Tiempo1;
        TimeSpan Tiempo2;
        TimeSpan Tiempo3;

        Tiempo1 = DateTime.Now.TimeOfDay;
        Tiempo2 = DateTime.Now.TimeOfDay;
        Tiempo3 = Tiempo1 - Tiempo2;

        System.Console.WriteLine("T1 " + Tiempo1);
        System.Console.WriteLine("T2 " + Tiempo2);
        System.Console.WriteLine("Df " + Tiempo3);

        Console.ReadKey();
    }
}