using System;

/*
// Se requiere desarrollar una aplicación que contenga información de 10 alumnos
// (expediente, nombre y carrera), la información debe ser almacenada como struct.
// Así mismo se requiere almacenar el nombre y el status (1 activa, 0 no activa)
// de 5 materias en un arreglo.
// El sistema tendrá un menú que permita realizar las siguientes operaciones y
// cada operación regresará al menú principal:
// 
//  a. El sistema deberá permitir introducir desde el teclado los datos de los 
//     alumnos (es posible introducir cargas completas o parciales cuando el
//     usuario inserte como valor un *) 
//  b. El sistema deberá permitir introducir desde teclado los datos de materias
//  c. Mostrar un reporte de los alumnos inscritos en orden descendente de
//     acuerdo a su expediente
//  d. Mostrar un reporte las materias cuyo status sea activo
//  e. Salir
*/

class Parcial1
{

    struct Alumno
    {
        public string Nombre;
        public string Carrera;
        public int Expediente;
    }
    
    struct Materia
    {
        public string Nombre;
        public bool Status;
    }

    static void Main()
    {
        char menu;
        Alumno[] alumno = new Alumno[10];
        Materia[] materias = new Materia[5];
        
        do
        {
            Console.Clear();
            System.Console.WriteLine("Ingrese una de las siguientes operaciones posibles");
            System.Console.WriteLine(" a. Introducir los datos de un alumno");
            System.Console.WriteLine(" b. Introducir o registrar materias los datos de materias");
            System.Console.WriteLine(" c. Mostrar reporte de los alumnos inscritos");
            System.Console.WriteLine(" d. Mostrar reporte de las materias activas");
            System.Console.WriteLine(" e. Salir");
            System.Console.Write(" > ");
            menu = (char) Console.Read();
            System.Console.WriteLine();

            switch (menu)
            {
                case 'a':
                    int selA;
                    System.Console.Write("¿A qué alumno quiere cambiar los datos [1 - 10]? ");
                    selA = int.Parse(Console.ReadLine()) - 1;
                    System.Console.WriteLine();

                    System.Console.WriteLine("Nombre: ");
                    alumno[selA].Nombre = Console.ReadLine();
                    System.Console.WriteLine("Carrera: ");
                    alumno[selA].Carrera = Console.ReadLine();
                    System.Console.WriteLine("Expediente: ");
                    alumno[selA].Expediente = int.Parse(Console.ReadLine());

                    break;
                case 'b':
                    int selB;
                    System.Console.WriteLine("\nIngrese las opciones para las materias existentes");
                    System.Console.WriteLine(" 1. Registrar");
                    System.Console.WriteLine(" 2. Modificar");
                    System.Console.Write(" > ");
                    selB = int.Parse(Console.ReadLine());

                    do
                    {
                        switch (selB)
                        {
                            case 1:

                                for (int i = 0; i < 5; i++)
                                {
                                    System.Console.Write("Nombre: ");
                                    materias[i].Nombre = Console.ReadLine();
                                    System.Console.Write("Status: ");
                                    materias[i].Status = bool.Parse(Console.ReadLine());
                                }

                                break;
                            case 2:
                                int numMat2;
                                System.Console.Write("¿De qué materia desea cambiar los datos [1 - 5]? ");
                                numMat2 = int.Parse(Console.ReadLine());

                                int opc;
                                System.Console.Write("¿Cuál campo quiere cambiar [Nombre(1)/Status(2)]? ");
                                opc = int.Parse(Console.ReadLine());

                                if (opc == 1)
                                {
                                    System.Console.Write(" > ");
                                    materias[numMat2].Nombre = Console.ReadLine();
                                }
                                if (opc == 2)
                                {
                                    System.Console.Write(" > ");
                                    materias[numMat2].Status = bool.Parse(Console.ReadLine());
                                }

                                break;
                            default:
                                System.Console.WriteLine("Ingrese una opción válida");
                                break;
                        }
                    } while (selB == 1 || selB == 2);

                    break;
                case 'c':
                    System.Console.WriteLine("Alumnos inscritos\n"); // En forma descendente de acuerdo a su número de expediente
                    int [] expedientes = new int[10];

                    for (int i = 0; i < 10; i++)
                    {
                        expedientes[i] = alumno[i].Expediente;
                    }
                    Array.Sort(expedientes);

                    foreach (int contador in expedientes)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            if (alumno[i].Expediente == contador)
                            {
                                System.Console.Write(alumno[i].Expediente + "\t");
                                System.Console.Write(alumno[i].Nombre + "\t\t");
                                System.Console.WriteLine(alumno[i].Carrera);                            
                            }
                        }
                    }
                    Console.ReadKey();
                    break;
                case 'd':
                    System.Console.WriteLine("Materias actualmente activas\n");
                    for (int i = 0; i < 5; i++)
                    {
                        if (materias[i].Status == true)
                        {
                            System.Console.WriteLine(materias[i].Nombre);
                        }
                    }
                    Console.ReadKey();
                    break;
                case 'e':
                    break;
                default:
                    System.Console.WriteLine("Ingrese una opción válida");
                    break;
            }

        } while (menu != 'e');
        
    }
}