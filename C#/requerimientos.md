# Sistema control de un modelo de línea de espera, mediante la adquisición, procesamiento y control de datos
## Requerimientos
* Para el desarrollo de este sistema, es necesario la utilización de tres sensores (entrada, servidor y salida) los cuales deben detectar la presencia de un objeto. El control de acceso al sistema se llevará a cabo mediante la instalación de un semáforo en la entrada que indicará al cliente(persona, solicitud, pieza, proceso, etc.) si es que se puede entrar o no a la instalación del servicio
* El sistema deberá permitir el acceso al sistema mediante la validación de la capacidad máxima del sistema que será definido inicialmente mediante la configuración inicial del sistema. Para que un cliente pueda entrar deberá validarse si hay espacio suficiente para el cliente en el sistema d elíneas de espera.
* El sistema de líneas de espera deberá mostrar en tiempo real las modificaciones d elas variables de desempeño del sistema todo el tiempo.
* Validar el sistema de tal manera que no se presenten más salidas, ni entradas al servidor que el número de entradas.
* Calcular las variables de desempeño:
    * Ls
    * Lq
    * Lss
    * Ws
    * Wq
    * Wss
    * L
    * M
    * P
* Mostrar gráficamente el número de personas que se encuentran en el sistema de colas.
* Graficar el comportamiento de las frecuencias de entrada y salidas por unidad de tiempo. Los n minutos que defina el usuario (estos gráficos solo se mostrarán cuando el usuario indique que desee verlos).
* Al macenar los datos leídos mediante los sensores (tiempo). Y guardar los datos obtenidos en archivos independientes (un archivo para cada sensor).
* Modelo funcional a escala que represente el sistema

## Modelo funcional
CD con todo el código fuente dle programa y la base de datos en un archivo SQL. El CD y su estuche deben estar debidamente etiquetados. El CD debe contener los siguientes elementos:
* Índice
* Requerimientos
* Objetivos
* Diagrama de componentes que muestre la iteración entre páginas
* Código fuente debidamente estructurado
* Manual de usuario
* Conslusiones
