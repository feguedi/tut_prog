import QtQuick 2.6
import QtQuick.Controls 2.0

Page {
    id: root

    header: ChatToolBar {
        Label {
            text: qsTr("Contactos")
            font.pixelSize: 20
            anchors.centerIn: parent
        }
    }

    ListView {
        id: vistaLista
        anchors.fill: parent
        topMargin: 48
        leftMargin: 48
        bottomMargin: 48
        rightMargin: 48
        spacing: 20

        model: SqlContactModel {}

        delegate: ItemDelegate {
            text: model.display
            width: vistaLista.width - vistaLista.leftMargin - vistaLista.rightMargin
            height: avatar.implicitHeight
            leftPadding: avatar.implicitWidth + 32
            onClicked: root.StackView.view.push("qrc:/ConversationPage.qml", { inConversationWith: model.display })

            Image {
                id: avatar
                source: "qrc:/" + model.display + ".png"
            }
        }
    }
}
