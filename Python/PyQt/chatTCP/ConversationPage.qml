import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Page {
    id: root

    property string inConversationWith

    header: ChatToolBar {
        ToolButton {
            // text: qsTr("Atrás")
            Image {
                id: back
                source: "qrc:/back.svg"
            }

            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.verticalCenter: parent.verticalCenter

            onClicked: root.StackView.view.pop()
        }

        Label {
            id: tituloPagina
            text: inConversationWith
            font.pointSize: 18
            anchors.centerIn: parent
        }
    }

    ColumnLayout {
        anchors.fill: parent

        ListView {
            id: vistaLista
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: pane.leftPadding + messageField.leftPadding
            displayMarginBeginning: 40
            displayMarginEnd: 40

            verticalLayoutDirection: ListView.BottomToTop
            spacing: 12
            model: SqlConversationModel {
                recipient: inConversationWith
            }
            delegate: Column {
                anchors.right: enviadoPorMi ? parent.right : undefined
                spacing: 6

                readonly property bool enviadoPorMi : model.recipient !== "Yo"

                Row {
                    id: messageRow
                    spacing: 6
                    anchors.right: enviadoPorMi ? parent.right : undefined

                    Image {
                        id: avatar
                        source: !enviadoPorMi ? "qrc:/" + model.author + ".png" : ""
                    }

                    Rectangle {
                        width: Math.min(messageText.implicitWidth + 24, vistaLista.width - avatar.width - messageRow.spacing)

                        Label {
                            id: messageText
                            text: model.message
                            color: enviadoPorMi ? "black" : "white"
                            anchors.fill: parent
                            anchors.margins: 12
                            wrapMode: Label.Wrap
                        }
                    }
                }

                Label {
                    id: textoTiempo
                    text: Qt.formatDateTime(model.timestamp, "d MM hh:mm")
                    color: "lightgrey"
                    anchors.right: enviadoPorMi ? parent.right : undefined
                }
            }

            ScrollBar.vertical: ScrollBar {}
        }

        Pane {
            id: pane
            Layout.fillWidth: true

            RowLayout {
                width: parent.width

                TextArea {
                    id: campoMensaje
                    Layout.fillWidth: true
                    placeholderText: qsTr("Escribe un mensaje...")
                    wrapMode: TextArea.Wrap
                }

                Button {
                    id: botonEnviar
                    // text: qsTr("Enviar")
                    Image {
                        id: enviar
                        source: "qrc:/enviar.svg"
                    }
                    enabled: campoMensaje.length > 0
                    onClicked: {
                        vistaLista.model.sendMessage(inConversationWith, campoMensaje.text);
                        campoMensaje.text = "";
                    }
                }
            }
        }
    }
}
