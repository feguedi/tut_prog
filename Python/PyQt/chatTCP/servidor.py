# *-* coding: utf-8 *-*
import select
import socket
from time import gmtime, strftime

# import sys

diccionario_host = {}


def impr_host_data():
    # Un mensaje regular puede lucir así:
    # m^Bob@128.2.46.60^-^ Mensaje de ejemplo^
    date = strftime("%d-%m-%Y %H:%M:S", gmtime())
    print("[", date, "] Cliente ")


def impr_nuevo_host(addr, nickname_socket):
    # Un mensaje de entrada puede lucir así:
    # j^Alice@128.2.46.60^-^ -^
    date = strftime("%d-%m-%Y %H:%M:%S", gmtime())
    print("j^{0}@{1}^-^ -^".format(str(nickname_socket), str(addr)))
    print("Entró a las ", date)


def prompt_salida(NICK, addr):
    prompt_salida = "\nm^{0}@{1}^-^ ".format(str(NICK), str(addr))
    return prompt_salida


def usuarios_lista(NICK, mensaje):
    prompt_salida = "\nm^{0}@servidor^-^ {1}^".format(str(NICK), mensaje)
    return prompt_salida


# Envia mensaje a todos los clientes conectados a excepción de quien lo envió
def broadcast_data(sock, mensaje):
    for socket in CONNECTION_LIST:
        if socket != server_socket and socket != sock:
            try:
                socket.send(prompt_salida(diccionario_host[sock.getpeername()], "") + mensaje)
            except:
                socket.close()
                CONNECTION_LIST.remove(socket)


# Envía una lista de todos los usuarios conectados
def broadcast_usuarios_activos(sock):
    for socket in CONNECTION_LIST:
        if socket == sock:
            for socket2 in CONNECTION_LIST:
                if socket2 != server_socket:
                    socket.send(usuarios_lista(diccionario_host[sock.getpeername()], "Activo"))
            break


def broadcast_nuevo_usuario(sock, mensaje):
    for socket in CONNECTION_LIST:
        if socket != server_socket and socket != sock:
            try:
                socket.send(usuarios_lista(diccionario_host[sock.getpeername()], mensaje))
            except:
                socket.close()
                CONNECTION_LIST.remove(socket)

if __name__ == '__main__':
    CONNECTION_LIST = []
    RECV_BUFFER = 4096
    PORT = 5000

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)  # Máximo de conexiones

    CONNECTION_LIST.append(server_socket)

    print("El servidor de chat inició en el puerto " + str(PORT) + "\n")

    while True:
        read_sockets, write_sockets, error_sockets = select.select(CONNECTION_LIST, [], [])

        for sock in read_sockets:
            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                CONNECTION_LIST.append(sockfd)

                nickname_socket = sockfd.recv(RECV_BUFFER)
                diccionario_host[sockfd.getpeername()] = nickname_socket
                impr_nuevo_host(addr, nickname_socket)

                broadcast_nuevo_usuario(sockfd, "+")
                broadcast_usuarios_activos(sockfd)

            else:
                try:
                    data = sock.recv(RECV_BUFFER)
                    if data == 'exit':
                        broadcast_nuevo_usuario(sock, "-")
                        print("p^Alice@128.2.46.60^-^ -^")
                        sock.close()
                        CONNECTION_LIST.remove(sock)
                        continue
                    broadcast_data(sock, data)
                    impr_host_data()

                except:
                    print("Error con el socket")
                    server_socket.close()
