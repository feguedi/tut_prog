import QtQuick 2.6
import QtQuick.Controls 2.0

ApplicationWindow {
    id: window
    width: 540
    height: 760
    visible: true

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: ContactPage {}
    }
}


