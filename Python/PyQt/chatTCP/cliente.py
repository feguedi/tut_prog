# *-* coding: utf-8 *-*
import socket, select, string, sys, color
from time import gmtime, strftime

"""
Escribe un simple servidor y un cliente de chat TCP usando sockets que use el
protocolo de chat descrito más adelante. El servidor debe ser capaz de leer
mensajes desde cada cliente y reenviar el mensaje mediante broadcast a todos
los clientes. El servidor debe leer el puerto desde la línea de comandos y
escuchar las peticiones en ese puerto. El servidor dee de soportar múltiples
conexiones concurrentes y generar un hilo para cada nueva conexión

Los mensajjes entre el cliente y el servidor deben tener el siguiente formato:
type^alias@hostIP^target^ mensaje^

Los tipos de mensajes pueden ser los siguientes:

m - Un mensaje regular, donde DONE el mensaje sólo debe ser reenviado
J - Un usuario entró. El mensaje debe ser un guión
P - Un usuario salió. El mensaje debe ser un guión

Un mensaje regular puede lucir así:
m^Bob@128.2.46.60^-^ Mensaje de ejemplo^

Un mensaje de entrada puede lucir así:
j^Alice@128.2.46.60^-^ -^

Un mensaje de salida puede lucir así:
p^Alice@128.2.46.60^-^ -^

El servidor sólo puede retransmitir el primer tipo de mensaje al cliente sin
hacer ninguna modificación, sin emgargo si un usuario entra o sale, se debe de
modificar el mensaje de acuerdo al siguiente ejemplo:

j^Bob@128.2.46.60^-^ ^

debe ser transformado así:

j^Bob@128.2.46.60^-^ Bob has joined
from uaq.mx(12:42 PM)^

Para embeber python en c++ y viceversa:
https://geekytheory.com/tutorial-embeber-python-en-c
https://docs.python.org/3/c-api/
http://www.bogotobogo.com/cplusplus/sockets_server_client_QT.php

Presentación de slideshare:
https://de.slideshare.net/cuerty/extendiendo-aplicaciones-en-c-y-c-con-python
"""

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usar : python3 {} [hostname | ip] port".format(sys.argv[0]))
        sys.exit()

    NICK = "___________"
    print("")

    while len(NICK) > 10:
        NICK = input("Ingresa tu nickname (10 caracteres máximo) : ")

    length = 10 - len(NICK)

    for i in range(0, length):
        NICK = "_" + NICK

    # Un mensaje regular puede lucir así:
    # m^Bob@128.2.46.60^-^ Mensaje de ejemplo^

    prompt = "m^{0}@{1}^-^ "
    host = sys.argv[1]
    port = int(sys.argv[2])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)

    try:
        s.connect((host, port))
    except:
        print("\nNo se puede conectar al servidor remoto")
        sys.exit()

    print("\n[+] Conectado.\n[+] Obteniendo usuarios en línea:\n")

    s.send(NICK)
    socket_list = [0, s]

    flag = True

    while flag:
        sys.stdout.write(prompt.format(host))
        sys.stdout.flush()

        read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [])

        for sock in read_sockets:
            if sock == s:
                data = sock.recv(4096)

                if not data:
                    print("\nDesconectado del chat")
                    flag = False
                    break
                else:
                    sys.stdout.write(data + "\n")
            else:
                mensaje = sys.stdin.readline().strip()
                if mensaje:
                    s.send(mensaje)
