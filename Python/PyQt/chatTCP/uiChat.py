# *-* coding: utf-8 *-*
import sys
import os.path
import cliente
import servidor
from PyQt5 import QtCore, QtGui, uic
from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow


class UIchat(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = uic.loadUi(os.path.join(os.path.dirname(__file__), 'chatcliente.ui'))
        self.ui.show()

    def setup_ui(self, usr, grp):
        """
        Objetos dentro de la interfaz:

         main_form  - QWidget
         btn_enviar - QPushButton
         txt_edit   - QPlainTextEdit
         lbl_titulo - QLabel
         txt_chat   - QLabel
        """
        self.ui.main_form.setWindowIcon()
        self.ui.lbl_titulo.setText('{0} - {1}'.format(str(usr), str(grp)))
        self.ui.txt_chat.clear()

    def act_lbl(self, text):
        self.ui.txt_chat
        return "Ola ke ase"

    def enviar(self):

        self.ui.txt_edit.clear()
        return "botonazo"


if __name__ == '__main__':
    app = QApplication(sys.argv)
    x = UIchat()
    sys.exit(app.exec_())
