QT += qml quick

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

TARGET = Filtros

INCLUDEPATH += ~Aplicaciones/opencv-3.2.0/include/opencv # Agregar la dirección de la librería de OpenCV

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
