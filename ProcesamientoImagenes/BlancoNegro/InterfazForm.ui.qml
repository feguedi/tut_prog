import QtQuick 2.4
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

Item {
    id: root
    property alias rutaOriginal: imgOriginal.source

    RowLayout {
        x: 69
        y: 41
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 30

        ColumnLayout {
            id: columnaOriginal
            spacing: 15

            Image {
                id: imgOriginal
                fillMode: Image.PreserveAspectFit
                asynchronous: true
                Layout.preferredHeight: 200
                Layout.preferredWidth: 200
                source: "qrc:/qtquickplugin/images/template_image.png"
            }

            Item {
                id: histOriginal
                Layout.preferredHeight: 73
                Layout.preferredWidth: 200
            }
        }

        ColumnLayout {
            id: columnaModificada
            spacing: 15

            Image {
                id: imgModificada
                fillMode: Image.PreserveAspectFit
                asynchronous: true
                Layout.preferredHeight: 200
                Layout.preferredWidth: 200
                source: "qrc:/qtquickplugin/images/template_image.png"
            }

            Item {
                id: histModificada
                Layout.preferredHeight: 73
                Layout.preferredWidth: 200
            }
        }
    }

    Button {
        id: agregarBoton
        width: 40
        height: 40
        flat: true
        contentItem: Rectangle {
            color: "transparent"
            Image {
                anchors.fill: parent
                source: "qrc:/img/add.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 8
    }
}
