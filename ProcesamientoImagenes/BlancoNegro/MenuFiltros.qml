import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.0

Item {
    id: item1
    width: 100
    height: 480

    ColumnLayout {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        anchors.top: parent.top
        anchors.topMargin: 40
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 40

        Button {
            id: brilloBoton
            height: 70; width: 70;
            Layout.preferredHeight: 70
            Layout.preferredWidth: 70
            flat: true
            contentItem: Image {
                anchors.fill: parent
                source: "qrc:/img/brightness.png"
                fillMode: Image.PreserveAspectFit
            }

        }

        Button {
            id: convolucionBoton
            height: 70; width: 70;
            Layout.preferredHeight: 70
            Layout.preferredWidth: 70
            flat: true
            contentItem: Image {
                anchors.fill: parent
                source: "qrc:/img/histogram.png"
                fillMode: Image.PreserveAspectFit
            }

        }

        Button {
            id: ruidoBoton
            height: 70; width: 70;
            Layout.preferredHeight: 70
            Layout.preferredWidth: 70
            flat: true
            contentItem: Image {
                anchors.fill: parent
                source: "qrc:/img/blur.png"
                fillMode: Image.PreserveAspectFit
            }

        }

        Button {
            id: histogramaBoton
            height: 70; width: 70;
            Layout.preferredHeight: 70
            Layout.preferredWidth: 70
            flat: true
            contentItem: Image {
                anchors.fill: parent
                source: "qrc:/img/black-white.png"
                fillMode: Image.PreserveAspectFit

            }
        }
    }

}
