import QtQuick 2.3
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.0

ApplicationWindow {
    id: window
    title: qsTr("Blanco y negro")
    width: Screen.desktopAvailableWidth / 2
    height: Screen.desktopAvailableHeight / 2

    StackView {
        anchors.fill: parent
        Label {
            anchors.fill: parent
            anchors.centerIn: parent
            text: qsTr("Ola ke ase")
        }
    }
}
