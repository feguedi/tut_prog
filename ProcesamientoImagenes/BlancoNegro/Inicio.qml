import QtQuick 2.3
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.0

Item {
    property url ruta;

    MouseArea{
        anchors.fill: parent
        ColumnLayout {
            anchors.fill: parent
            spacing: 5.0

            Label {
                text: qsTr("Selecciona la imagen")
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                color: "Black"
            }

            Button {
                id: selector
                text: qsTr("...")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: dialogo.open()
            }

        }

        FileDialog {
            id: dialogo
            title: "Selecciona una imagen"
            folder: shortcuts.pictures
            nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ]
            onAccepted: {
                console.log("Se abrirá la imagen: " + dialogo.fileUrl)
                ruta = dialogo.fileUrl
                dialogo.close()
            }
            onRejected: {
                console.log("Cancelado")
                dialogo.close()
            }
        }
    }

}
