QT += qml core quick quickcontrols2 charts

TARGET = Simple
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp

OTHER_FILES = *.qml

RESOURCES += res.qrc

DEFINES += QT_DEPRECATED_WARNINGS

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
